import React, { useEffect } from 'react';
import { Text } from 'react-native';
import { styles } from './style';
import { loadGoogleFont } from './style';

const MyComponent = () => {
  useEffect(() => {
    loadGoogleFont();
  }, []);

  return <Text style={styles.text}>Hello World!</Text>;
};

export default MyComponent;
