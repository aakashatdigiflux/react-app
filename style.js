import { StyleSheet, Platform, Linking } from 'react-native';

const styles = StyleSheet.create({
  text: {
    fontFamily: 'Poppins',
    fontSize: 16,
  },
});

const loadGoogleFont = async () => {
  await Linking.openURL('https://fonts.googleapis.com/css2?family=Poppins:wght@500&display=swap');
};

export { styles, loadGoogleFont };
